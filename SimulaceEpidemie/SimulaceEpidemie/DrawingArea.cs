﻿using Entity;
using System;
using System.Windows.Forms;

namespace SimulaceEpidemie
{
    public partial class DrawingArea : Form
    {
        public event Action ListNotInfectedChanged;

        public event Action ListInfectedChanged;

        public event Action ListResistantChanged;

        public event Action ListDeadChange;

        public MultiplePerson multiplePerson;
        public Timer worldClock;

        public bool resistantIsVisible;
        public bool deadIsVisible;



        public DrawingArea()
        {
            InitializeComponent();

            multiplePerson = new MultiplePerson(10, 1, 1.0, 1.0, 1.0);

            worldClock = new Timer();
            worldClock.Interval = 100;
            worldClock.Tick += WorldClockOnTick;
            worldClock.Start();
        }

        public DrawingArea(int numberOfPeople, int numberOfInfectiousPeople, int speedOfAnimation, double probabilityOfInfection, double probabilityOfDeath, double probabilityOfResistance, bool resistantIsVisible, bool deadIsVisible)
        {
            InitializeComponent();

            multiplePerson = new MultiplePerson(numberOfPeople, numberOfInfectiousPeople, probabilityOfInfection, probabilityOfDeath, probabilityOfResistance);

            worldClock = new Timer();
            worldClock.Interval = speedOfAnimation;
            worldClock.Tick += WorldClockOnTick;
            worldClock.Start();



            this.resistantIsVisible = resistantIsVisible;
            this.deadIsVisible = deadIsVisible;
        }

        private void WorldClockOnTick(object sender, EventArgs eventArgs)
        {
            multiplePerson.DecideIfPeopleDie();
            multiplePerson.DetectInfectedNeighbor();
            multiplePerson.DecideWhoWillBeResistant();
            multiplePerson.MoveThemAll(this);

            if (ListInfectedChanged != null)
            {
                ListInfectedChanged();
            }

            if (ListNotInfectedChanged != null)
            {
                ListNotInfectedChanged();
            }

            if (ListResistantChanged != null)
            {
                ListResistantChanged();
            }

            if (ListDeadChange != null)
            {
                ListDeadChange();
            }
        }

        private void DrawingArea_Load(object sender, EventArgs e)
        {
        }

        public void DrawingArea_Paint(object sender, PaintEventArgs e)
        {
            foreach (Person person in multiplePerson.ListoOfAllPeople)
            {
                if (person.StateOfPerson == Person.State.Resistant && resistantIsVisible)
                {
                    person.Draw(e);
                }
                if (person.StateOfPerson == Person.State.Dead && deadIsVisible)
                {
                    person.Draw(e);
                }
                if (person.StateOfPerson == Person.State.Infected || person.StateOfPerson == Person.State.Susceptible)
                {
                    person.Draw(e);
                }
            }
        }
    }
}