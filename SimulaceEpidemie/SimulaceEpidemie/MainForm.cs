﻿using Entity;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace SimulaceEpidemie
{
    public partial class MainForm : Form
    {
        private Timer timer;
        private DrawingArea drawingArea;
        private int notInfected;
        private int infected;
        private int resistant;
        private int dead;
        private bool allInfected;
        private bool allDead;
        private int tickCount;
        private bool timerIsStopped;

        public MainForm()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Tick += TimerTick;

            chart1.Series.RemoveAt(0);
            chart1.ChartAreas[0].BackColor = Color.Transparent;
            chart1.Legends[0].BackColor = Color.Transparent;
            chart1.ChartAreas[0].AxisX.Title = "Ticks";
            chart1.ChartAreas[0].AxisY.Title = "People";

            chart1.Series.Add("Susceptible");
            chart1.Series["Susceptible"].ChartType = SeriesChartType.Line;
            chart1.Series["Susceptible"].Color = Color.Green;
            chart1.Series["Susceptible"].BorderWidth = 2;

            chart1.Series.Add("Infected").ChartType = SeriesChartType.Line;
            chart1.Series["Infected"].ChartType = SeriesChartType.Line;
            chart1.Series["Infected"].Color = Color.Red;
            chart1.Series["Infected"].BorderWidth = 2;

            chart1.Series.Add("Resistant").ChartType = SeriesChartType.Line;
            chart1.Series["Resistant"].ChartType = SeriesChartType.Line;
            chart1.Series["Resistant"].Color = Color.Blue;
            chart1.Series["Resistant"].BorderWidth = 2;

            chart1.Series.Add("Dead").ChartType = SeriesChartType.Line;
            chart1.Series["Dead"].ChartType = SeriesChartType.Line;
            chart1.Series["Dead"].Color = Color.Black;
            chart1.Series["Dead"].BorderWidth = 2;

            checkBox1.CheckStateChanged += checkBox1_CheckedChanged;
            checkBox2.CheckStateChanged += checkBox2_CheckedChanged;

        }

        private void TimerTick(object sender, EventArgs e)
        {
            chart1.Series["Susceptible"].Points.AddY(notInfected);
            chart1.Series["Infected"].Points.AddY(infected);
            chart1.Series["Resistant"].Points.AddY(resistant);
            chart1.Series["Dead"].Points.AddY(dead);

            chart1.Update();
            label28.Text = tickCount.ToString();
            Refresh();

            if (!drawingArea.Visible || allInfected || allDead)
            {
                timer.Stop();
                timerIsStopped = true;
                drawingArea.worldClock.Stop();
                BringToFront();

                allInfected = false;
                allDead = false;
                tickCount = 0;
            }

            tickCount++;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label24.Text = trackBar1.Value.ToString();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar2.Value.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            chart1.Series["Susceptible"].Points.Clear();
            chart1.Series["Infected"].Points.Clear();
            chart1.Series["Resistant"].Points.Clear();
            chart1.Series["Dead"].Points.Clear();

            if (trackBar3.Value > 0)
            {
                double probabilityOfInfection = (double)trackBar4.Value / 100;
                double probabilityOfDeath = (double)trackBar6.Value / 100;
                double probabilityOfResistance = (double)trackBar5.Value / 100;
                bool resistantIsVisible = checkBox2.Checked;
                bool deadIsVisible = checkBox1.Checked;

                drawingArea = new DrawingArea(trackBar1.Value, trackBar2.Value, trackBar3.Value, probabilityOfInfection, probabilityOfDeath, probabilityOfResistance, resistantIsVisible, deadIsVisible);

                drawingArea.ListNotInfectedChanged += () =>
                {
                    label6.Text = drawingArea.multiplePerson.ListOfSusceptiblePeople.Count.ToString();
                    notInfected = drawingArea.multiplePerson.ListOfSusceptiblePeople.Count;
                };

                drawingArea.ListInfectedChanged += () =>
                {
                    label8.Text = drawingArea.multiplePerson.ListOfInfectedPeople.Count.ToString();
                    infected = drawingArea.multiplePerson.ListOfInfectedPeople.Count;
                };

                drawingArea.ListResistantChanged += () =>
                {
                    label14.Text = drawingArea.multiplePerson.ListOfResistantPeople.Count.ToString();
                    resistant = drawingArea.multiplePerson.ListOfResistantPeople.Count;
                };

                drawingArea.ListDeadChange += () =>
                {
                    label16.Text = drawingArea.multiplePerson.ListOfDeadPeople.Count.ToString();
                    dead = drawingArea.multiplePerson.ListOfDeadPeople.Count;
                };

                drawingArea.multiplePerson.GetInfo += (message, character) =>
                {
                    if (character == MultiplePerson.MessageCharacter.Infected)
                    {
                        richTextBox1.SelectionColor = Color.Red;
                        richTextBox1.AppendText(message);
                    }
                    else if (character == MultiplePerson.MessageCharacter.Dead)
                    {
                        richTextBox1.SelectionColor = Color.Black;
                        richTextBox1.AppendText(message);
                    }
                    else if (character == MultiplePerson.MessageCharacter.Resistant)
                    {
                        richTextBox1.SelectionColor = Color.Blue;
                        richTextBox1.AppendText(message);
                    }
                    else
                    {
                        richTextBox1.SelectionColor = Color.DeepPink;
                        richTextBox1.AppendText("Something went wrong :(" + Environment.NewLine);
                    }
                };

                drawingArea.multiplePerson.AllInfected += () =>
                {
                    richTextBox1.SelectionColor = Color.BlueViolet;
                    richTextBox1.AppendText("All infected event raised." + Environment.NewLine);
                    allInfected = true;
                };
                drawingArea.multiplePerson.AllDeadOrResistant += () =>
                {
                    richTextBox1.SelectionColor = Color.BlueViolet;
                    richTextBox1.AppendText("All dead or resistant event raised." + Environment.NewLine);
                    allDead = true;
                };

                drawingArea.Show();
                timer.Start();
                timerIsStopped = false;
            }
            else
            {
                MessageBox.Show("Speed of animation have to be more than 0!",
                    "Speed of animations", MessageBoxButtons.OK);
            }
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            if (drawingArea != null && trackBar3.Value > 0)
            {
                drawingArea.worldClock.Interval = trackBar3.Value;
                timer.Interval = trackBar3.Value;
            }
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            label10.Text = trackBar3.Value.ToString();
        }

        private void trackBar4_Scroll(object sender, EventArgs e)
        {
            label19.Text = trackBar4.Value.ToString();
        }

        private void trackBar6_Scroll(object sender, EventArgs e)
        {
            label26.Text = trackBar6.Value.ToString();
        }

        private void trackBar5_Scroll(object sender, EventArgs e)
        {
            label23.Text = trackBar5.Value.ToString();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (drawingArea != null)
            {
                drawingArea.resistantIsVisible = checkBox2.Checked;
                if (timerIsStopped)
                {
                    drawingArea.Refresh();
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (drawingArea != null)
            {
                drawingArea.deadIsVisible = checkBox1.Checked;
                if (timerIsStopped)
                {
                    drawingArea.Refresh();
                }
            }
        }
    }
}