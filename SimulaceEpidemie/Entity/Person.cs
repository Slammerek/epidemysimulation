﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Entity
{
    public class Person
    {
        public int ID { get; set; }
        public int X { get; set; }

        public int Y { get; set; }

        public enum State { Susceptible, Infected, Resistant, Dead }

        public static Random random = new Random();

        public State StateOfPerson { get; set; }

        public Rectangle Rectangle;

        // private Random random;
        private readonly int[] steps = { -10, 0, 10 };

        #region Constructors

        public Person(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Person(int x, int y, State stav, int id)
        {
            X = x;
            Y = y;
            StateOfPerson = stav;
            ID = id;
        }

        #endregion Constructors

        public void Draw(PaintEventArgs g)
        {   // maximalni rozmer: 370X, 350Y
            Rectangle = new Rectangle(X, Y, 10, 10);
            SolidBrush brush;

            switch (StateOfPerson)
            {
                case State.Infected:
                    brush = new SolidBrush(Color.Red);
                    break;

                case State.Susceptible:
                    brush = new SolidBrush(Color.Green);
                    break;

                case State.Resistant:
                    brush = new SolidBrush(Color.Blue);
                    break;
                case State.Dead:
                    brush = new SolidBrush(Color.Black);
                    break;                    

                default:
                    brush = new SolidBrush(Color.BlueViolet);
                    break;
            }

            g.Graphics.FillEllipse(brush, Rectangle);
        }

        public void Move()
        {
            int stepX = random.Next(0, 3);
            int stepY = random.Next(0, 3);

            if (Rectangle.Location.X <= 370 && Rectangle.Location.X >= 0)
            {
                X = X + steps[stepX];
            }

            else
            {
                if (Rectangle.Location.X > 370)
                {
                    X = 370;
                }
                if (Rectangle.Location.X < 0)
                {
                    X = 0;
                }
            }

            if (Rectangle.Location.Y <= 350 && Rectangle.Location.Y >= 0)
            {
                Y = Y + steps[stepY];
            }
            else
            {
                if (Rectangle.Location.Y > 350)
                {
                    Y = 350;
                }
                if (Rectangle.Location.Y < 0)
                {
                    Y = 0;
                }
            }

            Debug.WriteLine(Rectangle.Location.X + " : " + Rectangle.Location.Y);
        }
    }
}