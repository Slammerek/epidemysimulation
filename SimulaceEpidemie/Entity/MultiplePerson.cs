﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Entity
{
    public delegate void MessagePasser(string message, MultiplePerson.MessageCharacter character);

    public class MultiplePerson
    {
        public event MessagePasser GetInfo;

        public event Action AllInfected;

        public event Action AllDeadOrResistant;

        public string InfoMessage { get; private set; }

        public enum MessageCharacter { Infected, Resistant, Dead }

        public List<Person> ListOfSusceptiblePeople { get; private set; }

        public List<Person> ListOfInfectedPeople { get; private set; }

        public List<Person> ListoOfAllPeople { get; private set; }

        public List<Person> ListOfResistantPeople { get; private set; }

        public List<Person> ListOfDeadPeople { get; private set; }

        private static readonly Random x = new Random();

        /*
         * Width and Height of WinForm
         */
        private const int minX = 0;
        private const int maxX = 370;
        private const int minY = 0;
        private const int maxY = 350;

        private double probabilityOfInfection;
        private double probabilityOfDeath;
        private double probabilityOfResistance;

        public MultiplePerson(int countOfPeopleToGenerate, int countOfInfectedPoeple, double probabilityOfInfection, double probabilityOfDeath, double probabilityOfResistance)
        {
            ListOfSusceptiblePeople = new List<Person>();
            ListOfInfectedPeople = new List<Person>();
            ListoOfAllPeople = new List<Person>();
            ListOfResistantPeople = new List<Person>();
            ListOfDeadPeople = new List<Person>();

            GenerateSusceptiblePerson(countOfPeopleToGenerate);
            GenerateInfectedPerson(countOfInfectedPoeple);

            ListoOfAllPeople.AddRange(ListOfSusceptiblePeople);
            ListoOfAllPeople.AddRange(ListOfInfectedPeople);

            this.probabilityOfInfection = probabilityOfInfection;
            this.probabilityOfDeath = probabilityOfDeath;
            this.probabilityOfResistance = probabilityOfResistance;
        }

        #region Test of Transmitting infection

        //public MultiplePerson()
        //{
        //    ListOfPersons = new List<Person>();
        //    ListOfInfectiousPersons = new List<Person>();
        //    ListoOfAllPerson = new List<Person>();

        //    TESTGenerateInfected();
        //    TESTGeneretaeNormalPeople();

        //    ListoOfAllPerson.AddRange(ListOfPersons);
        //    ListoOfAllPerson.AddRange(ListOfInfectiousPersons);
        //}

        //private void TESTGenerateInfected()
        //{
        //    ListOfInfectiousPersons.Add(new Person(200, 200, Person.State.Infectious));
        //}

        //private void TESTGeneretaeNormalPeople()
        //{
        //    for (int i = 0; i < 40; i++)
        //    {
        //        for (int j = 0; j < 40; j++)
        //        {
        //            ListOfPersons.Add(new Person(i * 10, j * 10, Person.State.Susceptible));
        //        }
        //    }
        //}

        #endregion Test of Transmitting infection

        private int id = 0;

        private void GenerateInfectedPerson(int countOfInfectedPeople)
        {
            for (int i = 0; i < countOfInfectedPeople; i++)
            {
                int firstNumber = x.Next(minX, maxX);
                int firstRounded = Convert.ToInt32(Math.Round(firstNumber / 100d, 1) * 100);

                int secondNumber = x.Next(minY, maxY);
                int secondRounded = Convert.ToInt32(Math.Round(secondNumber / 100d, 1) * 100);

                ListOfInfectedPeople.Add(new Person(firstRounded, secondRounded, Person.State.Infected, id));

                id++;
            }
        }

        /*
         * Generate multiple instances of Person class
         */

        private void GenerateSusceptiblePerson(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int firstNumber = x.Next(minX, maxX);
                int firstRounded = Convert.ToInt32(Math.Round(firstNumber / 100d, 1) * 100);

                int secondNumber = x.Next(minY, maxY);
                int secondRounded = Convert.ToInt32(Math.Round(secondNumber / 100d, 1) * 100);

                ListOfSusceptiblePeople.Add(new Person(firstRounded, secondRounded, Person.State.Susceptible, id));

                id++;
            }
        }

        public void DetectInfectedNeighbor()
        {
            double result;
            foreach (Person person in ListoOfAllPeople)
            {
                int XLeftNeighbor = person.Rectangle.Location.X - 10;
                int XRightNeighbor = person.Rectangle.Location.X + 10;

                int YUpperNeighbor = person.Rectangle.Location.Y + 10;
                int YLowerNeighbor = person.Rectangle.Location.Y - 10;

                for (int i = 0; i < ListoOfAllPeople.Count; i++)
                {
                    // X-axis is moved by -10 and Y-axis is the same (left neighbor)
                    if (XLeftNeighbor == ListoOfAllPeople[i].Rectangle.Location.X && person.Rectangle.Location.Y == ListoOfAllPeople[i].Rectangle.Location.Y)
                    {
                        result = x.NextDouble();

                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    if (XRightNeighbor == ListoOfAllPeople[i].Rectangle.Location.X && person.Rectangle.Location.Y == ListoOfAllPeople[i].Rectangle.Location.Y)
                    {
                        result = x.NextDouble();

                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    if (YUpperNeighbor == ListoOfAllPeople[i].Rectangle.Location.Y && person.Rectangle.Location.X == ListoOfAllPeople[i].Rectangle.Location.X)
                    {
                        result = x.NextDouble();
                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    if (YLowerNeighbor == ListoOfAllPeople[i].Rectangle.Location.Y && person.Rectangle.Location.X == ListoOfAllPeople[i].Rectangle.Location.X)
                    {
                        result = x.NextDouble();

                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    // left lower diagonal pacient
                    if (XLeftNeighbor == ListoOfAllPeople[i].Rectangle.Location.X && YLowerNeighbor == ListoOfAllPeople[i].Rectangle.Location.Y)
                    {
                        result = x.NextDouble();
                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    // left upper diagonal pacient
                    if (XLeftNeighbor == ListoOfAllPeople[i].Rectangle.Location.X && YUpperNeighbor == ListoOfAllPeople[i].Rectangle.Location.Y)
                    {
                        result = x.NextDouble();
                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    // right lower diagonal pacient
                    if (XRightNeighbor == ListoOfAllPeople[i].Rectangle.Location.X && YLowerNeighbor == ListoOfAllPeople[i].Rectangle.Location.Y)
                    {
                        result = x.NextDouble();
                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                    // right upper diagonal pacient
                    if (XRightNeighbor == ListoOfAllPeople[i].Rectangle.Location.X && YUpperNeighbor == ListoOfAllPeople[i].Rectangle.Location.Y)
                    {
                        result = x.NextDouble();
                        if (result < probabilityOfInfection)
                        {
                            InfectIfNot(i, person, result);
                        }
                    }
                }
            }
        }

        /*
         *** Chance calculation ***
         * double probability = user input chance of infection
         * double result = random double between 0.0 - 1.0
         * if(result < probability){
         *    do stuff
         * }
         */

        public void MoveThemAll(Form form)
        {
            foreach (Person person in ListoOfAllPeople)
            {
                if (person.StateOfPerson != Person.State.Dead)
                {
                    person.Move();
                    form.Refresh(); 
                }
                
            }
        }

        private void InfectIfNot(int i, Person person, double probabilityOfInfection)
        {
            int prob = 100 - Convert.ToInt32(Math.Floor(probabilityOfInfection * 100));

            if (ListoOfAllPeople[i].StateOfPerson == Person.State.Infected &&
                person.StateOfPerson != Person.State.Dead &&
                person.StateOfPerson != Person.State.Infected &&
                person.StateOfPerson != Person.State.Resistant)
            {
                // infect person

                ListOfSusceptiblePeople.Remove(person);

                // if no susceptible people exists raise an event to force stopping

                person.StateOfPerson = Person.State.Infected;

                if (!ListOfInfectedPeople.Contains(person))
                {
                    ListOfInfectedPeople.Add(person);
                    InfoMessage = @"[ ID: " + person.ID + " ]" + " Person at coords[X,Y]: " + "[" + person.X + ", " + person.Y + "]" +
                                  " has been infected by person at: " + "[" +
                                  ListoOfAllPeople[i].X + ", " + ListoOfAllPeople[i].Y + "]" + "ID: " + ListoOfAllPeople[i].ID + " with probability of: " +
                                  prob + "%" + Environment.NewLine;

                    if (GetInfo != null)
                    {
                        GetInfo(InfoMessage, MessageCharacter.Infected);
                    }
                }
                if (ListOfSusceptiblePeople.Count <= 0)
                {
                    AllInfected();
                }
            }
        }

        public void DecideIfPeopleDie()
        {
            double result;
            int prob;

            foreach (Person person in ListoOfAllPeople)
            {
                result = x.NextDouble();

                if (person.StateOfPerson == Person.State.Infected)
                {
                    if (result < probabilityOfDeath)
                    {
                        person.StateOfPerson = Person.State.Dead;
                        ListOfInfectedPeople.Remove(person);

                        ListOfDeadPeople.Add(person);

                        prob = 100 - Convert.ToInt32(Math.Floor(result * 100));

                        InfoMessage = @"[ ID: " + person.ID + " ]" + " Person at coords[X,Y]: " + "[" + person.X + ", " + person.Y + "]" +
                                  " has died with probability of: " + prob + "%" + Environment.NewLine;

                        if (GetInfo != null)
                        {
                            GetInfo(InfoMessage, MessageCharacter.Dead);
                        }

                        if (ListOfInfectedPeople.Count <= 0)
                        {
                            AllDeadOrResistant();
                        }
                    }
                }
            }
        }

        public void DecideWhoWillBeResistant()
        {
            double result;
            int prob;

            foreach (Person person in ListoOfAllPeople)
            {
                result = x.NextDouble();

                if (person.StateOfPerson == Person.State.Infected)
                {
                    if (result < probabilityOfResistance)
                    {
                        person.StateOfPerson = Person.State.Resistant;
                        ListOfInfectedPeople.Remove(person);
                        ListOfResistantPeople.Add(person);

                        prob = 100 - Convert.ToInt32(Math.Floor(result * 100));

                        InfoMessage = @"[ ID: " + person.ID + " ]" + " Person at coords[X,Y]: " + "[" + person.X + ", " + person.Y + "]" +
                                  " has become resistant with probability of: " + prob+ "%" + Environment.NewLine;

                        if (GetInfo != null)
                        {
                            GetInfo(InfoMessage, MessageCharacter.Resistant);
                        }

                        if (ListOfInfectedPeople.Count <= 0)
                        {
                            AllDeadOrResistant();
                        }
                    }
                }
            }
        }

        /*
         * Method deciding if rest of not dead people yet still infected become resistant
         */
        //public void DecideIfPeopleBecomeResistant()
        //{
        //    double result;

        //    foreach (Person person in ListoOfAllPeople)
        //    {
        //        result = x.NextDouble();
        //        if (person.StateOfPerson == Person.State.Infected)
        //        {
        //            if (result < probabilityOfDeath)
        //            {
        //                person.StateOfPerson = Person.State.Dead;
        //                ListOfInfectedPeople.Remove(person);
        //                ListOfDeadPeople.Add(person);
        //            }
        //        }
        //    }
        //}

        //public void DecideIfDeadOrResistant()
        //{
        //    double result = x.NextDouble();
        //    foreach (Person person in ListoOfAllPeople)
        //    {
        //        if (person.StateOfPerson == Person.State.Infected)
        //        {
        //            int randomState = x.Next(1, 4);
        //            string randomlyChosenState = enumStates[randomState];

        //            person.StateOfPerson = (Person.State)Enum.Parse(typeof(Person.State), randomlyChosenState);

        //            if (randomlyChosenState == "Dead")
        //            {
        //                ListOfDeadPeople.Add(person);
        //                ListOfInfectedPeople.Remove(person);
        //            }
        //            else if (randomlyChosenState == "Resistant")
        //            {
        //                ListOfResistantPeople.Add(person);
        //                ListOfInfectedPeople.Remove(person);
        //            }
        //        }
        //    }
        //}

        //public void DetectInfectedNeighbor()
        //{
        //    foreach (Person person in ListoOfAllPerson)
        //    {
        //        int XLeftNeighbor = person.rect.Location.X - 10;
        //        int XRightNeighbor = person.rect.Location.X + 10;

        //        int YUpperNeighbor = person.rect.Location.Y + 10;
        //        int YLowerNeighbor = person.rect.Location.Y - 10;

        //        foreach (Person infectiousPerson in ListoOfAllPerson)
        //        {
        //            // Left neighbor = X-axis is moved by -10 and Y-axis is the same
        //            if (XLeftNeighbor == infectiousPerson.rect.Location.X && person.rect.Location.Y == infectiousPerson.rect.Location.Y)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }

        //            }
        //            // Right neighbor = X-axis is moved by 10 and Y-axis is the same
        //            if (XRightNeighbor == infectiousPerson.rect.Location.X && person.rect.Location.Y == infectiousPerson.rect.Location.Y)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //            // Upper neighbor = X-axis is the same Y-axis is moved by 10
        //            if (YUpperNeighbor == infectiousPerson.rect.Location.Y && person.rect.Location.X == infectiousPerson.rect.Location.X)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //            // Lower neighbor = X-axis is the same Y-axis is moved by -10
        //            if (YLowerNeighbor == infectiousPerson.rect.Location.Y && person.rect.Location.X == infectiousPerson.rect.Location.X)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //            // Left lower neighbor = X-axis is moved by -10 Y-axis is moved by -10
        //            if (XLeftNeighbor == infectiousPerson.rect.Location.X && YLowerNeighbor == infectiousPerson.rect.Location.X)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //            // Left upper neighbor = X-axis is moved by -10 Y-axis is moved by 10
        //            if (XLeftNeighbor == infectiousPerson.rect.Location.X && YUpperNeighbor == infectiousPerson.rect.Location.X)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //            // Right lower neighbor = X-axis is moved by 10 Y-axis is moved by -10
        //            if (XRightNeighbor == infectiousPerson.rect.Location.X && YLowerNeighbor == infectiousPerson.rect.Location.X)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //            // Right upper neighbor = X-axis is moved by 10 Y-axis is moved by 10
        //            if (XRightNeighbor == infectiousPerson.rect.Location.X && YUpperNeighbor == infectiousPerson.rect.Location.X)
        //            {
        //                if (infectiousPerson.StateOfPerson == Person.State.Infectious && person.StateOfPerson != Person.State.Infectious)
        //                {
        //                    person.StateOfPerson = Person.State.Infectious;
        //                }
        //            }
        //        }
        //    }
        //}
    }
}